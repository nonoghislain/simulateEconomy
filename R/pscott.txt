clear
x=(1:120)';
z=(rand(1,120)*9)';
y=sin(x+z);

// base cycle
yprime=hpfilter(y,50);

// real GDP
out=22968-0.02*x+1.8*yprime;

// potential GDP
nat_out=hpfilter(out,1600);

// output gap
gap=(log(out)-log(nat_out))*10000;

// inflation
infl=1.4-0.001*x+.26*gap+rand(120,1)*.4;

// interest rate
int=hpfilter(abs(1.211*infl+.95*hpfilter(rand(120,1),1600)),1);